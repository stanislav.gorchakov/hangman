#!/bin/bash

function run_app {
  python3 $APP_HOME/$APP_NAME/main.py ;
}

function make_checks {
  RESULT_CODE=0 ;

  echo RUN pylint ;
  pylint \
         -d duplicate-code \
         $APP_HOME/$APP_NAME \
         $TESTS_HOME \
         --rcfile=$APP_HOME/.pylintrc \
         || RESULT_CODE=1 ;

  echo RUN pycodestyle ;
  pycodestyle \
         $APP_HOME/$APP_NAME \
         $TESTS_HOME \
         || RESULT_CODE=1 ;

  echo RUN mypy ;
  mypy \
         $APP_HOME/$APP_NAME \
         $TESTS_HOME \
         --cache-dir=/dev/null \
         --config-file=$APP_HOME/tox.ini \
         || RESULT_CODE=1 ;

  echo RUN pytest ;
    pytest $TESTS_HOME -o cache_dir=/dev/null \
           -p no:cacheprovider \
           --cov=$APP_HOME/$APP_NAME \
           --cov-config=$APP_HOME/.coveragerc \
           || RESULT_CODE=1 ;

    echo RESULT_CODE=$RESULT_CODE ;
    exit $RESULT_CODE
}

case "$1" in
  run_app)
    run_app
    ;;
  make_checks)
    make_checks
    ;;
esac