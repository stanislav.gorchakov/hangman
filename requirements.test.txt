pycodestyle==2.5.0
pylint==2.5.2
mypy==0.790
pytest-cov==2.8.1
pytest-helpers-namespace==v2019.1.8
pytest-mock==2.0.0
pytest==5.4.1
testfixtures==6.14.0