import re
import random
from hangman.words import words


def check_word(word: str) -> bool:
    exp = re.compile(r"[a-zA-Z]+$")
    return bool(exp.match(word))


def check_input(input_: str) -> bool:
    return input_.isalpha() and len(input_) == 1


def get_word() -> str:
    word = random.choice(words)
    while not check_word(word):
        word = random.choice(words)

    return word.strip().lower()
