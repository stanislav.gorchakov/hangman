from typing import Optional
from hangman import utils

LIVES = 5
WRONG_INPUTS_MAX = 10


# pylint: disable=unsubscriptable-object
def game(word: Optional[str] = None) -> int:
    if word is None:
        word = utils.get_word()
    word_letters = set(word)
    mistakes = 0
    used_letters = set()
    wrong_inputs = 0

    while (len(word_letters) > 0
           and mistakes < LIVES
           and wrong_inputs < WRONG_INPUTS_MAX):
        user_letter = input('Guess a letter:\n').lower()
        if not utils.check_input(user_letter):
            wrong_inputs += 1
            print("Enter only letters one by one")
            continue
        used_letters.add(user_letter)
        if user_letter in word_letters:
            print('Hit!')
            word_letters.remove(user_letter)
        else:
            mistakes += 1
            print(f"Missed, mistake {mistakes} out of {LIVES}.")

        word_list = [letter if letter in used_letters else '*' for letter in word]
        word_masked = ''.join(word_list)
        print(f"The word: {word_masked}")

    if wrong_inputs == WRONG_INPUTS_MAX:
        print("You're really bad :)")
        return 1
    if mistakes == LIVES:
        print(f"You lost!\nWord was: {word}")
        return 1
    print('You won!')
    return 0


if __name__ == '__main__':
    game()
