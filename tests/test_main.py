from hangman.main import game


def test_fail_game(monkeypatch):
    monkeypatch.setattr('builtins.input', lambda _: "a")

    assert game()


def test_win_game(monkeypatch):
    monkeypatch.setattr('builtins.input', lambda _: "a")

    assert not game("aaa")


def test_max_wrong_inputs(monkeypatch):
    monkeypatch.setattr('builtins.input', lambda _: "aa")

    assert game()
