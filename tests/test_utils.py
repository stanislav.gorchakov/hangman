import pytest
from hangman.utils import check_word, check_input, get_word
from hangman.words import words


@pytest.mark.parametrize('word, check',
                         [
                             pytest.param('word', True),
                             pytest.param('WorD', True),
                             pytest.param('word1', False),
                             pytest.param('w|or*d', False),
                             pytest.param('wor-d', False),
                         ]
                         )
def test_check_word(word, check):
    assert check_word(word) is check


@pytest.mark.parametrize('input_, check',
                         [
                             pytest.param('w', True),
                             pytest.param('W', True),
                             pytest.param('1', False),
                             pytest.param('ww', False),
                             pytest.param('*', False),
                             pytest.param('\n', False),
                         ]
                         )
def test_check_input(input_, check):
    assert check_input(input_) is check


# pylint: disable=unused-argument
@pytest.mark.parametrize('execution_number', range(5))
def test_get_word(execution_number):
    word = get_word()
    assert isinstance(word, str)
    assert word in words
