FROM python:3.9.6-slim

ENV APP_NAME=hangman
ENV APP_HOME=/usr/local/$APP_NAME
ENV TESTS_HOME=$APP_HOME/tests
ENV PYTHONPATH=$PATH:$APP_HOME

COPY docker/entrypoint.sh /usr/local/bin
RUN chmod +x /usr/local/bin/entrypoint.sh
COPY requirements.test.txt $APP_HOME/requirements.test.txt
RUN pip install --no-cache-dir -r $APP_HOME/requirements.test.txt

ENTRYPOINT ["entrypoint.sh"]