# Hangman

## Запуск локально
```shell
git clone https://gitlab.com/stanislav.gorchakov/hangman.git
cd hangman
docker build --tag hangman .
docker run -it --rm -v `pwd`/hangman:/usr/local/hangman/hangman hangman run_app
```

## Запуск тестов локально
```shell
docker-compose -f docker-compose.tests.yaml up --build --exit-code-from=test
```